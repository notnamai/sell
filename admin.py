import __future__

from datetime import datetime
from typing import Optional

from marshmallow import Schema, fields, validate, ValidationError
from pydantic import BaseModel, validator, ValidationError as PydanticValidationError

from flask import Flask, render_template, request

from models import Ad
from models.base import session

app = Flask('', static_url_path='/static')


class AdSchema(Schema):
    id = fields.Int(dump_only=True)
    # имя рекламодателя
    advertiser = fields.Str(validate=validate.Length(max=20))  # type: ignore
    # текст рекламы
    ad_text = fields.Str(validate=validate.Length(max=500))  # type: ignore
    # ссылка на картинку для рекламы
    url_img = fields.Str(validate=validate.Length(max=100))  # type: ignore
    # статус новое, отправлено
    status = fields.Str(default='new')
    # дата создания
    create_ad = fields.DateTime(dump_only=True)
    # дата отправки
    date_sending = fields.DateTime(dump_only=True)
    # скольки людям было отправлено
    number_of_people = fields.Int(allow_none=True)


class AdSchemaPydantic(BaseModel):
    id: Optional[int]
    # имя рекламодателя
    advertiser: str
    # текст рекламы
    ad_text: str
    # ссылка на картинку для рекламы
    url_img: str
    # статус новое, отправлено
    status: str = "new"
    # дата создания
    create_ad: Optional[datetime]
    # дата отправки
    date_sending: Optional[datetime]
    # скольки людям было отправлено
    number_of_people: int

    class Config:
        orm_mode = True

    @validator('advertiser')
    def validate_advertiser(cls, v):
        if len(v) > 20:
            raise ValueError('Длина имени рекламодателя должна быть меньше 20 символов')
        return v

    @validator('ad_text')
    def validate_ad_text(cls, v):
        if len(v) > 500:
            raise ValueError('Длина текста рекламной компании должна быть меньше 500 символов')
        return v

    @validator('url_img')
    def validate_url_img(cls, v):
        if len(v) > 100:
            raise ValueError('Длина ссылки должна быть меньше 100 символов')
        return v


@app.route('/pydantic', methods=['GET', 'POST'])
def get_table_pydantic():
    errors = []
    if request.method == 'POST':
        try:
            ad_data: AdSchemaPydantic = AdSchemaPydantic(**request.form)
            ads = Ad(**ad_data.dict())
            session.add(ads)
            session.commit()
        except PydanticValidationError as error:
            errors = error
    ads = session.query(Ad).all()
    return render_template('index.html', ads=ads, errors=errors)


@app.route('/api/ads/pydantic', methods=['GET'])
def api_get_ads_pydantic():
    ads = session.query(Ad).all()
    return [AdSchemaPydantic.from_orm(i_ad).dict() for i_ad in ads]


@app.route('/', methods=['GET', 'POST'])
def get_table():
    errors = []
    if request.method == 'POST':
        try:
            ad_data: dict = AdSchema().load(request.form)
            ads = Ad(**ad_data)
            session.add(ads)
            session.commit()
        except ValidationError as error:
            errors = error
    ads = session.query(Ad).all()
    return render_template('index.html', ads=ads, errors=errors)


@app.route('/api/ads', methods=['GET'])
def api_get_ads():
    ads = session.query(Ad).all()
    return AdSchema().dump(ads, many=True)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
