import datetime

from sqlalchemy import Integer, Column, String, Text, DateTime

from models.base import Base, session


class Ad(Base):
    __tablename__ = 'ads'
    id = Column(Integer, primary_key=True)
    # имя рекламодателя
    advertiser = Column(String)
    # текст рекламы
    ad_text = Column(Text)
    # ссылка на картинку для рекламы
    url_img = Column(String)
    # статус новое, отправлено
    status = Column(String, default='new')
    # дата создания
    create_ad = Column(DateTime)
    # дата отправки
    date_sending = Column(DateTime)
    # скольки людям было отправлено
    number_of_people = Column(Integer)


def add_ad_table() -> None:
    add_ads = Ad(advertiser='Часы Rolex',
                 ad_text='Мы любим животных и стараемся поддерживать тех из них,'
                         ' кому не посчастливилось иметь ласковых хозяев и тёплый кров',
                 url_img='https://yandex.ru',
                 create_ad=datetime.datetime.now(),
                 number_of_people=3
                 )

    session.add(add_ads)
    session.commit()
