from sqlalchemy import (
    Column,
    Integer,
    String,
    )
from telebot.types import Message

from models.base import (
    Base,
    session,
    )


class Person(Base):

    __tablename__ = 'about_me'
    id = Column(Integer, primary_key=True)
    chat_id: int = Column(Integer)
    username: str = Column(String)
    first_name: str = Column(String)
    last_name: str = Column(String)


def add_person_table(message: Message) -> None:
    is_person = session.query(Person).filter(Person.chat_id == message.chat.id).first()
    if is_person is None:
        add_about_me = Person(chat_id=message.chat.id,
                              username=message.chat.username,
                              first_name=message.chat.first_name,
                              last_name=message.chat.last_name)

        session.add(add_about_me)
        session.commit()
