"""Этот файл ORM на основе PEEWEE."""
from datetime import datetime

from sqlalchemy import (
    Column,
    Integer,
    String,
    update, DateTime, func,
)

from models.base import (
    Base,
    session,
    )


class ApiHotels(Base):
    """Класс для создания таблицы."""

    __tablename__ = 'api_hotels'
    id = Column(Integer, primary_key=True)
    chat_id: int = Column(Integer)
    city: str = Column(String, nullable=True)
    min_price: int = Column(Integer, nullable=True)
    max_price: int = Column(Integer, nullable=True)
    create_at: datetime = Column(DateTime, server_default=func.now(), onupdate=func.current_timestamp())


def add_city(chat_id: int, city: str) -> None:
    """
    Функция добавляет город в таблицу.

    :param chat_id: int
    :param city: str
    """
    city = ApiHotels(
        chat_id=chat_id,
        city=city,
    )
    session.add(city)
    session.commit()


def delete_chat_id(chat_id: int) -> None:
    """
    Функция чистит таблицу.

    :param chat_id: int
    """

    session.query(ApiHotels).filter(ApiHotels.chat_id == chat_id).delete()
    session.commit()


def update_min_price(chat_id: int, min_price: int) -> None:
    """
    Функция добавляет в таблицу min_price.

    :param chat_id: int
    :param min_price: int
    """
    session.execute(
        update(ApiHotels).
        where(ApiHotels.chat_id == chat_id).
        values(min_price=min_price)
        )
    session.commit()


def update_max_price(chat_id: int, max_price: int) -> None:
    """
    Функция добавляет в таблицу max_price.

    :param chat_id: int
    :param max_price: int
    """
    session.execute(
        update(ApiHotels).
        where(ApiHotels.chat_id == chat_id).
        values(max_price=max_price)
        )
    session.commit()

