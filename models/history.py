from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    String,
    func,
    )

from models.base import Base


class History(Base):

    __tablename__ = 'histories'
    id = Column(Integer, primary_key=True)
    datatime = Column(DateTime, server_default=func.now(), onupdate=func.current_timestamp())
    chat_id: int = Column(Integer)
    city: str = Column(String, nullable=True)
    min_price: int = Column(Integer, nullable=True)
    max_price: int = Column(Integer, nullable=True)
    result_hotels = Column(String)
