from sqlalchemy import create_engine
from sqlalchemy.orm import (
    declarative_base,
    sessionmaker,
    )

engine = create_engine('sqlite:///python_test.db', echo=True, connect_args={'check_same_thread': False})
Base = declarative_base()
DBSession = sessionmaker(bind=engine)
session = DBSession()
