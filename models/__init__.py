from .api_hotels import ApiHotels

from .history import History
from .person import Person
from .ad import Ad

from .base import (
    Base,
    engine,
    )

Base.metadata.create_all(engine)
