"""Это файл с классами для телеграмм-бота."""
from typing import Any


class PriceError(Exception):
    """Собственный класс, создан для отлова ошибок."""

    def __init__(self, message: str) -> None:
        """
        Конструктор добавляет полe message.

        :param message: поле содержит сообщение об ошибке
        """
        self.message = message
        super().__init__(message)


class ParamsSearchHotels(object):
    """Класс создан для установления цен."""

    def __init__(self, city: str) -> None:
        """
        Конструктор добавляет поля city, min_price, max_price.

        :param city: содержит поле название города
        """
        self.city = city
        self._min_price = None
        self._max_price = None

    @property
    def min_price(self) -> Any:
        """
        Метод для уст минимальной цены.

        :return: поле с мин ценой
        """
        return self._min_price

    @min_price.setter
    def min_price(self, digit: Any) -> None:
        try:
            digit = int(digit)
        except ValueError:
            raise PriceError('Невозможно перевести в число')
        if digit >= 0:
            self._min_price = digit
        else:
            raise PriceError('Минимальная цена должна быть больше нуля')

    @property
    def display_info_city(self) -> str:
        """
        Метод для уст города.

        :return: поле с названием города
        """
        return self.city

    @property
    def max_price(self) -> Any:
        """
        Метод для уст максимальной цены.

        :return: поле макс цены
        """
        return self._max_price

    @max_price.setter
    def max_price(self, digit: Any) -> None:
        try:
            digit = int(digit)
        except ValueError:
            raise PriceError('Невозможно перевести в число')
        if digit >= self._min_price:
            self._max_price = digit
        else:
            raise PriceError(
                'Максимальная цена должна быть больше минимальной цены',
            )
