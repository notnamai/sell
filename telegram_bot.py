"""
Это телеграмм-бот для поиска отелей.

Он использует RapidApi.com
Перед использованием просьба прочитать файл README.md
"""

from functools import wraps
from typing import (
    Any,
    Callable,
    Dict,
    Tuple,
    TypeVar,
    cast,
    )

import requests
import telebot
from decouple import config
from geopy.geocoders import Nominatim
from telebot.types import Message

from bot_logger import logger
from models import ApiHotels
from models.ad import add_ad_table
from models.api_hotels import (
    add_city,
    delete_chat_id,
    update_max_price,
    update_min_price,
    )
from models.base import session
from models.history import History
from models.person import (
    Person,
    add_person_table,
    )
from service import (
    ParamsSearchHotels,
    PriceError,
    )

KEY = config('TELEGRAM_BOT_TOKEN')

bot = telebot.TeleBot(KEY)

geolocator = Nominatim(user_agent='my-app')

user_dict: Dict[Any, Any] = {}

MyCallable = TypeVar('MyCallable', bound=Callable)


def error_checking_assignment_min_price(message: Message, min_price: Any) -> None:
    """
    Функция принимает параметр min_price.

    Записывает параметр min_price в базу данных, проверяет min_price на правильность ввода
    Обращается к словарю user_dict, чтобы забрать из него данные.

    :param message: Message
    :param min_price: int
    """
    chat_id = message.chat.id
    update_min_price(chat_id, min_price)
    try:
        param_search = user_dict[chat_id]
    except KeyError:
        city = session.query(ApiHotels.city).filter(ApiHotels.chat_id == chat_id).one()

        if city is None:
            msg = bot.reply_to(message, 'Укажите пожалуйста город')
            bot.register_next_step_handler(msg, write_name_city)
            return
        else:
            param_search = ParamsSearchHotels(city)
            user_dict[chat_id] = param_search

    try:
        param_search.min_price = min_price
    except PriceError as ans:
        msg = bot.reply_to(message, ans.message)
        bot.register_next_step_handler(msg, min_price_step)
        return


def error_checking_assignment_max_price(message: Message, max_price: Any) -> None:
    """
    Функция принимает параметр max_price.

    Записывает параметр max_price в базу данных, проверяет max_price на правильность ввода
    Обращается к словарю user_dict, чтобы забрать из него данные.

    :param message: Message
    :param max_price: int
    """
    chat_id = message.chat.id
    update_max_price(chat_id, max_price)
    try:
        param_search = user_dict[chat_id]
    except KeyError:
        city = (session.query(ApiHotels.city).filter(ApiHotels.chat_id == chat_id).one())
        min_price = ' '.join(
            str(ans) for ans in session.query(ApiHotels.min_price).
            filter(ApiHotels.chat_id == chat_id).one()
        )
        if city is None:
            msg = bot.reply_to(message, 'Укажите пожалуйста город')
            bot.register_next_step_handler(msg, write_name_city)
            return
        else:
            user_dict[chat_id] = ParamsSearchHotels(city)
            param_search = user_dict.get(chat_id)
            param_search.min_price = min_price

    try:
        param_search.max_price = max_price
    except PriceError as ans:
        msg = bot.reply_to(message, ans.message)
        bot.register_next_step_handler(msg, max_price_step)
        return


def cather(func: MyCallable) -> MyCallable:
    """
    Занимается поиском ошибок.

    Тут есть описание

    :param func:тут тоже есть описание
    :return:тут тоже есть описание
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as exc:
            logger.exception(exc)
            return exc

    return cast(MyCallable, wrapper)


@bot.message_handler(commands=['history'])
def send_history(message: Message) -> None:
    """
    Функция запускает бота.

    Необходимо прописать '/history'.

    :param message: Что-то тут есть
    """
    response_to_user = ' '.join(
        str(ans) for ans in session.query(History.city, History.min_price, History.max_price, History.result_hotels).
        filter(History.chat_id == message.chat.id).all()
    )
    if response_to_user is not None:
        bot.reply_to(message, response_to_user)
    else:
        msg = bot.reply_to(
            message,
            'История пока пуста',
        )
        bot.register_next_step_handler(msg, write_name_city)


@bot.message_handler(commands=['aboutme'])
def about_me(message: Message) -> None:
    """
    Функция запускает бота.

    Необходимо прописать '/aboutme'.

    :param message: Что-то тут есть
    """
    response_to_user = ' '.join(
        str(ans) for ans in session.query(Person.username, Person.first_name, Person.last_name).
        filter(Person.chat_id == message.chat.id).first()
    )
    bot.reply_to(message, response_to_user)


@bot.message_handler(commands=['help', 'start'])
def send_welcome(message: Message) -> None:
    """
    Функция запускает бота.

    Необходимо прописать '/start'.

    :param message: Что-то тут есть
    """
    msg = bot.reply_to(
        message,
        'enter the name of the city',
    )
    bot.register_next_step_handler(msg, write_name_city)
    add_person_table(message)


@cather
def write_name_city(message: Message) -> None:
    """

    Функция уст название города.

    Принимает от пользователя название города

    :param message: Что-то тут есть.
    """
    chat_id = message.chat.id
    city = message.text
    if city.isalpha():
        delete_chat_id(chat_id)
        add_city(chat_id, city)
        add_ad_table()
        param_search = ParamsSearchHotels(city)
        user_dict[chat_id] = param_search
        msg = bot.reply_to(message, 'min price in $')
        bot.register_next_step_handler(msg, min_price_step)
    else:
        msg = bot.reply_to(message, 'Укажите пожалуйста город')
        bot.register_next_step_handler(msg, write_name_city)
        return


@cather
def min_price_step(message: Message) -> None:
    """Функция уст мин цену.

    Принимает от пользователя мин цену

    :param message: что-то тут есть
    """
    min_price = message.text
    if min_price.isdigit():
        error_checking_assignment_min_price(message, min_price)
        msg = bot.reply_to(message, 'max price in $')
        bot.register_next_step_handler(msg, max_price_step)
    else:
        msg = bot.reply_to(message, 'Укажите цифру')
        bot.register_next_step_handler(msg, min_price_step)
        return


@cather
def max_price_step(message: Message) -> None:
    """Функция уст макс цену.

    Принимает от пользователя макс цену

    :param message: что-то тут есть
    """
    max_price = message.text
    if max_price.isdigit():
        error_checking_assignment_max_price(message, max_price)
        bot.reply_to(message, 'Looking for options')
        find_location(message)
    else:
        msg = bot.reply_to(message, 'Укажите цифру')
        bot.register_next_step_handler(msg, max_price_step)
        return


def find_location(message: Message) -> Tuple[Any, Any]:
    """
    Функция принимает название города.

    И переводит название города в координаты.

    :param message: текст
    :return: None
    """
    chat_id = message.chat.id
    param_search = user_dict[chat_id]
    location = geolocator.geocode('{city}'.format(city=param_search.display_info_city))
    try:
        location_latitude = location.latitude
        location_longitude = location.longitude
        get_settings_to_request(message, location_latitude, location_longitude)
        return location_latitude, location_longitude
    except AttributeError:
        msg = bot.reply_to(message, 'Вы указали неверный город, пожалуйста напишите город еще раз')
        bot.register_next_step_handler(msg, write_name_city)


def get_settings_to_request(message: Message, location_latitude: Any, location_longitude: Any) -> Any:
    """
    Функция устанавливает настройки в request запрос.

    Принимает долготу и широту и устанавливает доп параметры в querystring.

    :param message: текст
    :param location_latitude: принимает широту
    :param location_longitude: принимает долготу
    :return: Возвращает все необходимое для запроса к API
    """
    chat_id = message.chat.id
    param_search = user_dict[chat_id]
    url = 'https://hotels-com-provider.p.rapidapi.com/v1/hotels/nearby'
    querystring = {
        'latitude': '{latitude}'.format(latitude=location_latitude),
        'currency': 'USD',
        'longitude': '{longitude}'.format(longitude=location_longitude),
        'checkout_date': '2022-08-27',
        'sort_order': 'STAR_RATING_HIGHEST_FIRST',
        'checkin_date': '2022-08-25',
        'adults_number': '1',
        'locale': 'en_US',
        'guest_rating_min': '4',
        'star_rating_ids': '3,4,5',
        'children_ages': '4,0,15',
        'page_number': '1',
        'price_min': '{min_price}'.format(min_price=param_search.min_price),
        'accommodation_ids': '20,8,15,5,1',
        'theme_ids': '14,27,25',
        'price_max': '{max_price}'.format(max_price=param_search.max_price),
        'amenity_ids': '527,2063',
    }

    headers = {
        'X-RapidAPI-Key': config('key'),
        'X-RapidAPI-Host': 'hotels-com-provider.p.rapidapi.com',
    }
    final_answer(message, url, querystring, headers)
    return url, querystring, headers


@cather
def final_answer(message: Message, url: str, querystring: Dict, headers: Dict) -> None:
    """
    Функция собирает данные отправленные пользователем через Telegram.

    И подставляет их в request запрос для API. Возвращает готовый ответ
    в телеграмм-бот.

    :param message: какой-то текст
    :param url: какой-то текст
    :param querystring: текст
    :param headers: текст1
    """
    response = requests.request(
        'GET', url, headers=headers, params=querystring, timeout=10,
    )
    if response is None:
        msg = bot.reply_to(message, 'Укажите корректные данные')
        bot.register_next_step_handler(msg, write_name_city)
    result_hotels = []
    response_to_user = ''
    if response is not None:
        result_hotels = [
            '{city}: {price}'.format(city=i_hotel['name'], price=i_hotel['ratePlan']['price']['current'])
            for i_hotel in response.json()['searchResults']['results']
        ]
        response_to_user = '\n'.join(result_hotels)
    bot.reply_to(message, response_to_user)
    add_history = History(chat_id=message.chat.id,
                          city=' '.join(
                              str(ans) for ans in session.query(ApiHotels.city).
                              filter(ApiHotels.chat_id == message.chat.id).one()
                          ),
                          min_price=' '.join(
                              str(ans) for ans in session.query(ApiHotels.min_price).
                              filter(ApiHotels.chat_id == message.chat.id).one()
                          ),
                          max_price=' '.join(
                              str(ans) for ans in session.query(ApiHotels.max_price).
                              filter(ApiHotels.chat_id == message.chat.id).one()
                          ),
                          result_hotels='\n'.join(result_hotels),
                          )
    session.add(add_history)
    session.commit()


bot.enable_save_next_step_handlers(delay=2)

bot.load_next_step_handlers()

bot.infinity_polling()
