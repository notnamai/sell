"""
Это logger через loguru.

Замена print-ов.
"""

import sys

from loguru import logger

FORMAT = '%(pathname)s:%(lineno)d %(asctime) [ %(levelname)s ] : %(message)s'

logger.add(sys.stderr, format=FORMAT, filter='my_module', level='INFO')

logger.add('bot_app_log.log')
